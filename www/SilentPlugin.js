var exec    = require('cordova/exec');
var cordova = require('cordova');

function SilentPlugin() {

}

SilentPlugin.prototype.requestPermissions = function(successCallback, errorCallback) {
  cordova.exec(successCallback, errorCallback, "SilentPlugin", "requestPermissions", []);
}

SilentPlugin.prototype.status = function(successCallback, errorCallback) {
  cordova.exec(successCallback, errorCallback, "SilentPlugin", "status", []);
}

SilentPlugin.prototype.setSilent = function(successCallback, errorCallback) {
  cordova.exec(successCallback, errorCallback, "SilentPlugin", "setSilent", []);
}

SilentPlugin.prototype.setDefault = function(successCallback, errorCallback) {
  cordova.exec(successCallback, errorCallback, "SilentPlugin", "setDefault", []);
}

SilentPlugin.install = function() {
  if(!window.plugins) {
    window.plugins = {};
  }

  window.plugins.SilentPlugin = new SilentPlugin();

  return window.plugins.SilentPlugin;
}

cordova.addConstructor(SilentPlugin.install);
